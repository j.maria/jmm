# Avengers EndGame - Joel maria Montes

* Part0: [Pagina Html](https://j.maria.gitlab.io/jmm/Part0/index.html)
* Part1: [Pagina Html i CSS](https://j.maria.gitlab.io/jmm/Part1/index.html)
* Part2.1: [Template - Original](https://j.maria.gitlab.io/jmm/Part2.1-Original/index.html)
* Part2.2: [Template - JMM](https://j.maria.gitlab.io/jmm/Part2.2-JMM/index.html)
* Part3: [Bootstrap](https://j.maria.gitlab.io/jmm/Part3/index.html)
* Extra: [SASS](https://j.maria.gitlab.io/jmm/Extra/style.scss)

## Llistat de totes les propietats utilitzades.

1. font-family
2. margin
3. padding
4. color
5. display: none;
6. display: flex;
7. position: fixed;
8. position: relative;
9. position: absoluted;
10. text-align: center;
11. background-color
12. width
13. height
14. flex-grow
15. flex-wrap: wrap;
16. border
17. border-radius
18. text-decoration
19. transition
20. color
21. font-size
22. aling-items
